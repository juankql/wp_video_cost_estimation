(jQuery)(
	function($){
		
		
   	   	calculate = function () {
   	   		var data = new FormData();
			data.append('action', 'calculate');
			data.append('video_qty',$('#video_qty_slider').val()); 
			data.append('video_length',$('#video_length_slider').val()*60); 

   	   		$.ajax({
				url: ajax_object.ajax_url+'?action=calculate',
				type: "post",
				dataType: "json",
				data: data,
				contentType: false,
				processData: false,
				cache:false,
				success : function( response ) {
					if (response.type == 'ok') {
						//Box next to sliders
						$('#video_lenght_box').val(response.video_price + ' ' + response.currency);
						$('#video_qty_box').val(response.vide_script_voice_price + ' ' + response.currency); 
						//Summary
						$('#total_price_container').html(response.total + ' ' + response.currency);
						//Savings
						$('#video_average').html(response.average_length + '  s'); 
						$('#normal_price').html(response.average_price + ' ' + response.currency); 
						$('#saved_price').html(response.save + ' ' + response.currency); 
						$('#price_per_video').html(response.price_pr_video + ' ' + response.currency);     
					} 
				}
			});  
   	   	}    	 
   		
   		calculate(); 
	
		$('.downvalue').click ( function ( e ) {
		 	var id_slide = $(this).data('id_slider');
			var actual_value = $('#'+id_slide).val();
			var step = $('#'+id_slide).attr('step'); 
			actual_value -= step;
			if ( actual_value < $('#'+id_slide).attr('min') ) {
				actual_value = $('#'+id_slide).attr('min');
			}
			var display_box = $('#'+id_slide).data('id_display_box');  
			$('#'+id_slide).val(actual_value); 
			$('#'+display_box).val(actual_value);
			calculate();  
		});
        
        $('.upvalue').click ( function ( e ) {
		 	var id_slide = $(this).data('id_slider');
			var actual_value = parseFloat($('#'+id_slide).val());
			var step = parseFloat($('#'+id_slide).attr('step')); 
			actual_value = actual_value + step;                          
			if ( actual_value > $('#'+id_slide).attr('max') ) {
				actual_value = $('#'+id_slide).attr('max');
			}
			var display_box = $('#'+id_slide).data('id_display_box');  
			$('#'+id_slide).val(actual_value); 
			$('#'+display_box).val(actual_value); 
			calculate();  
		});
		
		$('.slide').change( function () {
			calculate();
		})
		
		
	}
);
