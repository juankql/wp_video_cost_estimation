<?php
/*
Plugin Name: Video Cost Estimation
Description: Displays a form to estimate the price of creating one set of animated videos and compare it with the option to buy the videos separately. Showing the user how much can save buying a set of animated videos against buying the videos each appart.
Version: 1.0.0
Author: [WP Kraken] Juan Carlos Quevedo Lusson
Text Domain: wpk-video-cost-estimation

License: GPL v3

Weigth Lost Calculator
Copyright (C) 2017 WP Kraken

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Prevent direct file access
defined( 'ABSPATH' ) or exit;

if(!class_exists( 'WP_Video_Cost_Estimator' ) ) {
 	class WP_Video_Cost_Estimator {
		 		
 		public function __construct (){
 			add_action('admin_menu', array($this, 'ft_view_menu'));   
			add_action( 'admin_init', array( $this,'ft_register_settings'));
			add_action( 'wp_ajax_calculate', array($this,'calculate_cost')); // for logged users
			add_action( 'wp_ajax_nopriv_calculate', array($this,'calculate_cost')); // for non logged users
			
			add_action('wp_enqueue_scripts', array($this,'ft_enqueue_styles'));
			add_action('wp_enqueue_scripts', array($this,'ft_enqueue_scripts'));
			
			add_action( 'plugins_loaded', array( $this, 'my_plugin_load_plugin_textdomain') );   
 			
			add_shortcode('wl_video_cost_estimation', array($this, 'wl_calculator_form'));
			
 		}
 		
 		public function calculate_cost () {
 			
 			$videos_qty = intval( $_POST['video_qty'] ); 
 			$video_time = intval($_POST['video_length']); 
 			$segments_length = get_option('segment_length',15);
 			$min_video_length = get_option('min_video_length',30); 
 			$video_price_first_segment = get_option('first_segment_price',50000);
 			$video_price_extra = get_option('extra_segments_price',10000);
 			$extra_script_price = get_option('extra_script_price', 10000);
 			$extra_voice_price = get_option('extra_voice_price', 5000);
 			
 			$segments = $video_time/$segments_length;
 			if( $segments < 2 ) {
 				$segments = 2;
 			}
 			
 			$video_price = $video_price_first_segment + $video_price_extra * ( $segments - 2 );
 			
 			
 			
 			$extra_scripts_voice = $videos_qty - 1;
 			$average_video_length = $video_time/$videos_qty;
 			$segments_average = $average_video_length/$segments_length;
 			if( $segments_average < 2 ) {
 				$segments_average = 2;
 			}
 			$average_video_price = $video_price_first_segment + $video_price_extra * ( $segments_average - 2 );  
 			          
 			$video_lenght_min = $video_time / 60;
 			$total_price = $video_price + ( $extra_script_price * $extra_scripts_voice ) + ( $extra_voice_price * $extra_scripts_voice );
 	        $normal_price = $average_video_price * $videos_qty;                              
 	        $save =  $normal_price - ($total_price);
 	        
            $response['type'] = 'ok';
			$response['total'] = round($total_price,2);
			$response['normal_price'] = round($normal_price,2);
			$response['save'] = round($save,2); 
			$response['average_length'] = round($average_video_length,2);
			$response['extra_scripts_voice'] = $extra_scripts_voice;
			$response['extra_script_price'] = round($extra_script_price * $extra_scripts_voice,2);
			$response['extra_voice_price'] = round($extra_voice_price * $extra_scripts_voice,2);
			$response['video_length_min'] = round($video_lenght_min,2);
			$response['video_price'] = round($video_price,2);
			$response['vide_script_voice_price'] = $response['extra_script_price'] + $response['extra_voice_price'];
			$response['currency'] = get_option('currency','NOK'); 
			$response['average_price'] = round($average_video_price,2);
			$response['price_pr_video'] =round( $total_price / $videos_qty , 2);
			
			header('Content-Type: application/json');
			echo json_encode( $response );                       
			wp_die();	
 		}
		
 		//shortcode
 		public function wl_calculator_form () {
 		   include_once('views/frontend_interface.php');
 		}
 		
 		public function ft_enqueue_styles() {
			wp_enqueue_style('wpvideo_styles', plugins_url('/',__FILE__).'ft_style.css');
			wp_enqueue_style('bootstrap4css', plugins_url('/',__FILE__).'bootstrap-4.0.0-alpha.5-dist/css/bootstrap.min.css', array(), '4.0.0-alpha5');  
			wp_enqueue_style('font-awesome', plugins_url('/',__FILE__).'css/font-awesome.min.css');  
			
		}

		public function ft_enqueue_scripts() {
			wp_enqueue_script('bootstrap4js', plugins_url('/',__FILE__).'bootstrap-4.0.0-alpha.5-dist/js/bootstrap.min.js', array('jquery'), '4.0.0-alpha5');
			wp_enqueue_script('jswebshim' , plugins_url('/', __FILE__).'includes/webshim-1.16.0/polyfiller.js', array('jquery'));  
			wp_enqueue_script('wpvideo_scripts', plugins_url('/',__FILE__).'wpwl_calculator.js', array('jquery','bootstrap4js','jswebshim'));
			
			$data = array(
				'ajax_url'   => admin_url('admin-ajax.php'),
			);

			wp_localize_script( 'wpvideo_scripts', 'ajax_object', $data );
		}

 		public function ft_admin_view_settings(){
			// Checking if the user has priviledges for managing options else show a warning.
			if ( !current_user_can( 'manage_options' ) ) {
				wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
			}
			include('views/admin_view_settings.php');
			
		}
		
 		public function ft_register_settings() {
 			register_setting( 'wl_estimator-settings', 'segment_length');
			register_setting( 'wl_estimator-settings', 'first_segment_price');
			register_setting( 'wl_estimator-settings', 'extra_segments_price');
			register_setting( 'wl_estimator-settings', 'extra_script_price');
			register_setting( 'wl_estimator-settings', 'extra_voice_price');
			register_setting( 'wl_estimator-settings', 'video_max_time' );
			register_setting( 'wl_estimator-settings', 'currency');  
			register_setting( 'wl_estimator-settings', 'min_video_length');     
		}
 		
 		public function ft_view_menu() {
			add_options_page( '[WP Kraken] Video Cost Estimation', '[WP Kraken] Video Cost Estimation', 'manage_options', 'wl_estimator-settings', array($this,'ft_admin_view_settings') );
		}
		
		function my_plugin_load_plugin_textdomain() {
		    load_plugin_textdomain( 'wpk-video-cost-estimation', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
		}
		
 	}
 	
 	$wpwl_estimator = new WP_Video_Cost_Estimator();
}     


