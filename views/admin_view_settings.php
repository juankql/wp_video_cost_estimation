<div class="wrap">
	<h2 style="text-align:center;padding-bottom:15px;">[WP Kraken] Video Cost Estimation Plugin Configuration</h2>
	<form action="options.php" method="post" enctype="multipart/form-data">
	<?php
 		settings_fields('wl_calculator-settings');
	?> 
		<table class="form-table">
			<tr>
				<td scope="row"><label for="segment_length"><?php _e( 'Segment length' , 'wpk-video-cost-estimation' ) ?></label></td>
				<td><input type="text" name="segment_length" id="segment_length" placeholder="<?php _e( 'Segment length' , 'wpk-video-cost-estimation' ) ?>" value="<?php echo get_option('segment_length', 15); ?>" style="width:100%;" ></td>
			</tr>
			<tr>
				<td scope="row"><label for="min_video_length"><?php _e( 'Lowest video length (in seconds)' , 'wpk-video-cost-estimation' ) ?></label></td>
				<td><input type="text" name="min_video_length" id="min_video_length" placeholder="<?php _e( 'Lowest video length (in seconds)' , 'wpk-video-cost-estimation' ) ?>" value="<?php echo get_option('min_video_length',30); ?>" style="width:100%;" ></td>
			</tr>
			<tr>
				<td scope="row"><label for="video_max_time"><?php _e( 'Max video length (in seconds)' , 'wpk-video-cost-estimation' ) ?></label></td>
				<td><input type="text" name="video_max_time" id="video_max_time" placeholder="<?php _e( 'Max video length' , 'wpk-video-cost-estimation' ) ?>" value="<?php echo get_option('video_max_time', 600); ?>" style="width:100%;" ></td>
			</tr>
			<tr>
				<td scope="row"><label for="first_segment_price"><?php _e( 'First Segment price' , 'wpk-video-cost-estimation' ) ?></label></td>
				<td><input type="text" name="first_segment_price" id="first_segment_price" placeholder="<?php _e( 'First Segment price' , 'wpk-video-cost-estimation' ) ?>" value="<?php echo get_option('first_segment_price', 50000); ?>" style="width:100%;" ></td>
			</tr>
			<tr>
				<td scope="row"><label for="extra_segments_price"><?php _e( 'Extra segments price' , 'wpk-video-cost-estimation' ) ?></label></td>
				<td><input type="text" name="extra_segments_price" id="extra_segments_price" placeholder="<?php _e( 'Extra segments price' , 'wpk-video-cost-estimation' ) ?>" value="<?php echo get_option('extra_segments_price',10000); ?>" style="width:100%;" ></td>
			</tr>
			<tr>
				<td scope="row"><label for="extra_script_price"><?php _e( 'Extra scripts price' , 'wpk-video-cost-estimation' ) ?></label></td>
				<td><input type="text" name="extra_script_price" id="extra_script_price" placeholder="<?php _e( 'Extra scripts price' , 'wpk-video-cost-estimation' ) ?>" value="<?php echo get_option('extra_script_price', 10000); ?>" style="width:100%;" ></td>
			</tr>
			<tr>
				<td scope="row"><label for="extra_voice_price"><?php _e( 'Extra voiceover price' , 'wpk-video-cost-estimation' ) ?></label></td>
				<td><input type="text" name="extra_voice_price" id="extra_voice_price" placeholder="<?php _e( 'Extra voiceover price' , 'wpk-video-cost-estimation' ) ?>" value="<?php echo get_option('extra_voice_price',5000); ?>" style="width:100%;" ></td>
			</tr>
			<tr>
				<td scope="row"><label for="currency"><?php _e( 'Currency' , 'wpk-video-cost-estimation' ) ?></label></td>
				<td><input type="text" name="currency" id="currency" placeholder="<?php _e( 'Currency' , 'wpk-video-cost-estimation' ) ?>" value="<?php echo get_option('currency','NOK'); ?>" style="width:100%;" ></td>
			</tr>
		</table>
		<?php 
		do_settings_sections('wl_calculator-settings'); 
		@submit_button();?>
	</form>
</div>