<script>
    webshims.setOptions("forms-ext", {
    	"replaceUI": true,
		"range": {
			"calcTrail": false,
			"classes": "show-activevaluetooltip show-ticklabels"
		}
	});

	webshims.polyfill("forms forms-ext");
</script>
<div class="row" id="estimation_form">
	<div class="row">
		<div class="col" id="step1">
			<div class="row">
				<h1 class="h1"><?php _e('Video Cost Estimator', 'wpk-video-cost-estimation'); ?></h1>                              
			</div>
			<div class="row">
				<div class="">
					<div class="range_container">
						<div class="column col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label class="aria-label-saving"><?php _e('Video length', 'wpk-video-cost-estimation'); ?></label>     
						</div>
					  	<div class="column col-xs-12 col-md-11 col-sm-11 col-lg-11">
							<div class="button-and-slider-container">
						  		<div class="button-container decrease-button column col-xs-4 col-md-1 col-sm-1 col-lg-1">
						    		<button class="downvalue" data-id_slider = "video_length_slider"><i class="fa fa-minus"></i></button>
						  		</div>
							  	<div class="column col-xs-4 col-md-10 col-sm-10 col-lg-10">
									<input id='video_length_slider' list="video_length-list" class="slide" data-range-calc-trail="false" data-id_display_box="video_lenght_box" style="width:100%;" step="<?php echo get_option('segment_length',15)/60;?>" min="<?php echo get_option('min_video_length',30)/60;?>" max="<?php echo get_option('video_max_time',600)/60;?>" tabindex="-1" type="range" value="<?php echo get_option('segment_length',30)/60;?>">
									<datalist id="video_length-list">
										<select>
											<?php
												$i=0; 
												for($i=0;$i<=get_option('video_max_time',600)/60;$i+=get_option('segment_length',15)/60){  
													if( ( ( $i*60 ) % 60 == 0 ) || ( ( $i*60 ) % 60 )==30 ){
														$label = intval($i).':'. (($i*60) % 60);
														if((($i*60) % 60) == 0) {
															$label.="0";
														}
														//$label = round($i,2);
													} else {
														$label = "";
													}
													?>
													<option value="<?php echo round($i,2);?>" label="<?php echo $label;?>"></option> 	
											 <?php	
												}
											?>
										</select>
									</datalist>
							  	</div>
						  		<div class="button-container increase-button column col-xs-4 col-md-1 col-sm-1 col-lg-1">
						    		<button class="upvalue" data-id_slider = "video_length_slider"><i class="fa fa-plus"></i></button>
						  		</div>
							</div>
					  	</div>
					  	<div class="floating-right-align-container column col-xs-12 col-md-1 col-sm-1 col-lg-1">
							<div class="saving-input-box">
						  		<form class="">
						    		<input id="video_lenght_box" type="tel" class="box">
						  		</form>
							</div>
					  	</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="">
					<div class="range_container">
						<div class="column col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label class="aria-label-saving"><?php _e('How many videos (up to 10)', 'wpk-video-cost-estimation'); ?></label>     
						</div>
					  	<div class="column col-xs-12 col-md-11 col-sm-11 col-lg-11">
							<div class="button-and-slider-container">
						  		<div class="button-container decrease-button column col-xs-4 col-md-1 col-sm-1 col-lg-1">
						    		<button class="downvalue" data-id_slider = "video_qty_slider"><i class="fa fa-minus"></i></button>
						  		</div>
							  	<div class="column col-xs-4 col-md-10 col-sm-10 col-lg-10">
									<input class="input slide" id='video_qty_slider' data-id_display_box="video_qty_box" style="width:100%;"  list="video_qty-list" step="1" min="1" max="10" tabindex="-1" type="range" value="1">
									<datalist id="video_qty-list">
										<select>
											<option value="1" label="1"></option>
											<option value="2" label="2"></option>
											<option value="3" label="3"></option>
											<option value="4" label="4"></option>
											<option value="5" label="5"></option> 
											<option value="6" label="6"></option>
											<option value="7" label="7"></option>
											<option value="8" label="8"></option>
											<option value="9" label="9"></option>
											<option value="10" label="10"></option>
										</select>
									</datalist>
							  	</div>
						  		<div class="button-container increase-button column col-xs-4 col-md-1 col-sm-1 col-lg-1">
						    		<button class="upvalue" data-id_slider = "video_qty_slider"><i class="fa fa-plus"></i></button>
						  		</div>
							</div>
					  	</div>
					  	<div class="floating-right-align-container column col-xs-12 col-md-1 col-sm-1 col-lg-1">
							<div class="saving-input-box">
						  		<form class="">
						    		<input id="video_qty_box" type="tel" class="box">
						  		</form>
							</div>
					  	</div>
					</div>
				</div>
			</div>
			<br/>
			<div id="saving" class="ft_summary col">
		   		<div class="row">
					<h2 class="h1"><?php _e('Savings', 'wpk-video-cost-estimation'); ?></h2>                              
				</div>
				<div class="row">
					<div class="column col-xs-12 col-sm-6 col-md-6 col-lg-6">
						Average video length (in seconds)
					</div> 
					<div class="column col-xs-12 col-sm-3 col-md-3 col-lg-3" >
						
					</div>
					<div class="column col-xs-12 col-sm-3 col-md-3 col-lg-3" id="video_average">
						
					</div>                            
				</div>	
				<div class="row">
					<div class="column col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<?php _e('Normal price', 'wpk-video-cost-estimation'); ?>
					</div> 
					<div class="column col-xs-12 col-sm-3 col-md-3 col-lg-3">
						
					</div>
					<div class="column col-xs-12 col-sm-3 col-md-3 col-lg-3" id="normal_price">
						
					</div>                            
				</div>
				<div class="row">
					<div class="column col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<?php _e('Your price per video', 'wpk-video-cost-estimation'); ?>
					</div> 
					<div class="column col-xs-12 col-sm-3 col-md-3 col-lg-3">
						
					</div>
					<div class="column col-xs-12 col-sm-3 col-md-3 col-lg-3" id="price_per_video">
						
					</div>                            
				</div>		
				<div class="row">
					<div class="column col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<?php _e('Saved', 'wpk-video-cost-estimation'); ?>
					</div> 
					<div class="column col-xs-12 col-sm-3 col-md-3 col-lg-3">
						
					</div> 
					<div class="column  col-xs-12 col-sm-3 col-md-3 col-lg-3" id="saved_price">
						
					</div>                            
				</div>	
			</div>
			<div id="summary" class="ft_summary col">
		   		<div class="row">
					<h2 class="h1"><?php _e('Summary', 'wpk-video-cost-estimation'); ?></h2>                              
				</div>
				<div class="row">
					<div class="column col-xs-12 col-sm-6 col-md-6 col-lg-6">
						Total price
					</div> 
					<div class="column col-xs-12 col-sm-3 col-md-3 col-lg-3">
						
					</div> 
					<div class="column  col-xs-12 col-sm-3 col-md-3 col-lg-3" id="total_price_container">
						
					</div>                            
				</div>	
			</div>
		</div>
	</div>
</div>